import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
import Layout from '@/layout'

export const constantRoutes = [
  {
    path: '/',
    component: Layout,
    redirect: '/home',
    children: [
        {
            path: 'home',
            component: () => import('@/views/Calculator'),
            name: 'Home',
            meta: { title: 'Home'}
        }
      ]
  },

  {
    path: '/profile',
    component: Layout,
    redirect: '/profile/index',
    children: [
        {
            path: 'index',
            component: () => import('@/views/Profile'),
            name: 'Profile',
            meta: { title: 'Profile'}
        }
    ]
  },

  // 404 page must be placed at the end !!!
  {
    path: '*',
    component: Layout,
    redirect: '/error-page/404',
    children: [
        {
            path: '404',
            component: () => import('@/views/error-page/404'),
            name: '404',
            meta: { title: '404'}
        }
      ]
  },
]

const createRouter = () => new Router({
  mode: 'history', // require service support
  base: '/',
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router

