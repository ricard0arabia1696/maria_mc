const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  name: state => state.user.name,
  roles: state => state.user.roles,
  user_id: state => state.user.user_id,
  permission_routes: state => state.permission.routes,
  is_pw_temp: state => state.user.is_pw_temp,
  login_loading: state => state.app.login_loading,
  modal_loading: state => state.talktomaria.modal_loading,
}
export default getters
