import { login, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'
import VueJwtDecode from 'vue-jwt-decode'

const state = {
  token: getToken(),
  name: '',
  roles: [],
  user_id: '',
  is_pw_temp: ''
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_USER_ID: (state, user_id) => {
    state.user_id = user_id
  },
  SET_PW_TEMP: (state, is_pw_temp) => {
    state.is_pw_temp = is_pw_temp
  },
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password }).then(response => {
        const data = response.data
        commit('SET_TOKEN', data.data)
        setToken(data.data)
        resolve(data.is_pw_temp)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      const decodedJwtToken = VueJwtDecode.decode(state.token) // decode jwt token
      getInfo(decodedJwtToken.sys_id).then(response => {
        const data = response.data.data[0]
        if (!data) {
          reject('Verification failed, please Login again.')
        }

        var roles = decodedJwtToken.userPolicies
        var name = data.fname

        // roles must be a non-empty array
        if (!roles || roles.length <= 0) {
          reject('getInfo: roles must be a non-null array!')
        }

        var rolesKeys = Object.keys(roles);
        commit('SET_ROLES', rolesKeys)
        commit('SET_NAME', name)
        commit('SET_USER_ID', decodedJwtToken.sys_id)
        commit('SET_PW_TEMP', data.is_pw_temp)

        resolve({
          roles: rolesKeys
        })
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit }) {
    return new Promise((resolve) => {
      commit('SET_TOKEN', '')
      commit('SET_USER_ID', [])
      commit('SET_ROLES', [])
      commit('SET_NAME', '')
      removeToken()
      resetRouter()
      resolve()
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      removeToken()
      resolve()
    })
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

