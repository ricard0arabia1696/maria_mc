import router from './router'
import store from './store'
//import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie

//NProgress.configure({ showSpinner: false }) // NProgress Configuration


router.beforeEach(async(to, from, next) => {
  // start progress bar
  //NProgress.start()

  // determine whether the user has logged in
  const hasToken = getToken()

  if (hasToken) {

    console.log('user has token', to)
    next()


    // if (to.path === '/login') {
    //   // if is logged in, redirect to the home page
    //   next({ path: '/' })
    //   NProgress.done() // hack: https://github.com/PanJiaChen/vue-element-admin/pull/2939
    // } else {
    //   // determine whether the user has obtained his permission roles through getInfo
    //   const hasRoles = store.getters.roles && store.getters.roles.length > 0
    //   if (hasRoles) {
    //     next()
    //   } else {
    //     try {
    //       // get user info
    //       // note: roles must be a object array! such as: ['admin'] or ,['developer','editor']
    //       const { roles } = await store.dispatch('user/getInfo')

    //       // generate accessible routes map based on roles
    //       const accessRoutes = await store.dispatch('permission/generateRoutes', roles)

    //       // dynamically add accessible routes
    //       router.addRoutes(accessRoutes)

    //       // hack method to ensure that addRoutes is complete
    //       // set the replace: true, so the navigation will not leave a history record
    //       next({ ...to, replace: true })
    //     } catch (error) {
    //       // remove token and go to login page to re-login
    //       await store.dispatch('user/resetToken')
    //       console.log('error in permission')
    //       next(`/login?redirect=${to.path}`)
    //       NProgress.done()
    //     }
    //   }
    // }
  } else {
    console.log('user has no token')
    /* has no token*/
    console.log('inside eles epermission')
    var loginForm = {
        username: 'jonathan.serban217@gmail.com',
        password: 'nxtgen2020'
    }

    store.dispatch('user/login', loginForm).then(res => {
        console.log('res = ', res)
        next('/home')
       
    }).catch(error => {
        console.log('error = ', error)
        next('/error')
    })
  }
})

router.afterEach(() => {
  // finish progress bar
  //NProgress.done()
})
