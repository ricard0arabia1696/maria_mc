import axios from 'axios'
import { getToken } from '@/api/auth'

export async function getCompany(value) {
    var token = await getToken()
    
    return axios.get('https://new-los-api-wivlpotrwa-an.a.run.app/v1/newlos/maria/mc/salary/'+value, { 
            'headers': { 'authtoken': token } 
    });
}