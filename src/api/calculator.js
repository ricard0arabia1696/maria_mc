import request from '@/utils/request'

export function fetchBrand() {
  return request({
    url: 'https://new-los-api-wivlpotrwa-an.a.run.app/v1/newlos/maria/mc/brand',
    method: 'get'
  })
}

export function fetchModel(brand) {
  return request({
    url: 'https://new-los-api-wivlpotrwa-an.a.run.app/v1/newlos/maria/mc/model/' + brand,
    method: 'get'
  })
}

export function fetchModelPrice(modelId) {
  return request({
    url: 'https://new-los-api-wivlpotrwa-an.a.run.app/v1/newlos/maria/mc/price/' + modelId,
    method: 'get'
  })
}