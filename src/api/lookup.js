import axios from 'axios'
import { getToken } from '@/api/auth'

class LookupAPI {
    async get(value,id='') {
        let token = await getToken()
        let result = []
        let lookup = ''

        switch(value){
            case 'issue_authority':
                lookup = 'values/09698665-6aa1-4e06-80bd-ea2227ccb7bc';
                break;
            case 'type_of_ownership':
                lookup = 'values/63789a49-fba2-4ac6-91a3-173b0c2578db';
                break;
            case 'loan_term':
                lookup = 'values/82e1ea94-52f2-452c-9744-08eea0d9b2ba';
                break;
            case 'civil_status':
                lookup = 'values/c9ce10db-f067-4c87-91df-7b289b2a5e39';
                break;
            case 'source_of_income':
                lookup = 'values/cabd75be-19c9-4e65-879f-adc361f57877';
                break;
            case 'document_name':
                lookup = 'values/ce922ecb-1ba5-49c8-9b22-3b3ddc671125';
                break;
            case 'loan_purpose':
                lookup = 'values/d515b249-cedb-4361-b6db-152bde24515c';
                break;
            case 'nationality':
                lookup = 'values/dd567703-588b-4e9d-88bf-7cb354dc42b2';
                break;
            case 'nature_of_work':
                lookup = 'values/fb3808b9-5adc-4cfd-9adc-42a84fa2e53c';
                break;
            case 'loan_type':
                lookup = 'values/5acffbf8-d1dd-444f-8cf6-84372d5be7be';
                break;
            case 'credit_type':
                lookup = 'values/feb13a9e-6e9a-4511-8faa-81af9a144a99';
                break;
            case 'relationship':
                lookup = 'values/808199bc-24c7-4e0c-a731-381b7c823092';
                break;
            default:
                lookup = value;
                if(id) lookup += '/' + id;
        }

        await axios.get('https://new-los-api-wivlpotrwa-an.a.run.app/v1/newlos/lookup/'+lookup, { 
            'headers': { 'authtoken': token } 
        }).then(response => {
            response.data.data.map(function(key) {
                switch(value){
                    case 'brgy':
                        result.push({
                            'value' : key.SysId,
                            'text' : key.Title
                        })
                        break;
                    default:
                        result.push({
                            'value' : (typeof key.MappingId !== 'undefined') ? key.MappingId : key.value,
                            'text' : (typeof key.Title !== 'undefined') ? key.Title : key.display,
                        })
                }
            });
        });
        return result
    }
}

export default LookupAPI