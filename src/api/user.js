import request from '@/utils/request'

export function login(data) {
  return request({
    url: 'https://new-los-api-wivlpotrwa-an.a.run.app/v1/newlos/auth/getToken',
    method: 'post',
    data
  })
}

export function getInfo(userId) {
  return request({
    url: 'https://new-los-api-wivlpotrwa-an.a.run.app/v1/newlos/users/' + userId,
    method: 'get',
  })
}

export function changePassword(data, userId) {
  return request({
    url: 'https://new-los-api-wivlpotrwa-an.a.run.app/v1/newlos/users/change/pw/' + userId,
    method: 'put',
    data
  })
}
